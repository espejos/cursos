package com.espejos.holamundo;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    //Constantes en ramas
    private static final String PATH_START = "start";
    private static final String PATH_MESSAGE = "message";
    FirebaseDatabase dtBase = FirebaseDatabase.getInstance();
    final DatabaseReference reference = dtBase.getReference(PATH_START).child(PATH_MESSAGE);
    //Variables de
    @BindView(R.id.etMessage)
    EditText etMessage;
    @BindView(R.id.btnSend)
    Button btnSend;
    @BindView(R.id.tvMessage)
    TextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tvMessage.setText(dataSnapshot.getValue(String.class));
                etMessage.setText(dataSnapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(MainActivity.this, "Error al consultar en firebase",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @OnClick(R.id.btnSend)
    public void onViewClicked() {
        if(etMessage.getText().toString() != ""){
            reference.setValue(etMessage.getText().toString().trim());
            alert("Se ha cambiado el valor de firebase", null, null);
            etMessage.setText("");
        }else alert("No puede estar vacío", null, null);
    }

    public void alert(String _texto, String _titulo, String _tipo){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(_texto);
    }
}
